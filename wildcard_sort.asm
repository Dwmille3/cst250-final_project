# Input arguments:
#    $a0 - starting memory address of the array to be sorted
#    $a1 - number of elements in array

wildcard_sort:
    # Initialization
    li $t1, 1           # Decimal number 1
    li $t4, 4           # Decimal number 4
    move $t6, $t1       # counter1 = 1
    beq $a1, $t1, Done2	# If the length of the array equals one, jump to Done

    selection_sort:
        move $t5, $t6  # Counter = counter1
        move $v0, $a0  # Copy array pointer a0 to v0
        lw $s0, 0($v0) # Load first value to s0
        move $v1, $v0  # v1 is the position of the element s0 took the value from
                       # Beginning of the loop

    SORT:
        addu $v0, $v0, $t4    # Add 4 to the array pointer v0
        addu $t5, $t5, $t1    # Counter ++
        lw $t0, 0($v0)        # load the value to t0
        beq $t0, $s0, if_less # If t0 = s0, jump to if_less to skip the next process
        nop

        slt $s1, $s0, $t0     # If s0 < t0; set s1 to 1, otherwise set it to zero
        bne $s1, $0, if_less  # If s1 != 0, jump to if_less
        nop                   # Otherwise, keep going

        move $s0, $t0         # change s0 to t0
        move $v1, $v0         # load the position of the element s0 took the value from

        if_less:
            bne $t5, $a1, SORT    # if counter != a1, jump to SORT
            nop                   # Otherwise continue

            lw $t0 0($a0)         # Load the value from a0
            sw $s0, 0($a0)        # Swap the value between a0
            sw $t0, 0($v1)        # and v1
            addu $a0, $a0, $t4    # Add 4 to the array pointer a0
            addu $t6, $t6, $t1    # Counter1 ++
            bne $t6, $a1, selection_sort # If counter1 != a1, jump to selection_sort
            nop   # Otherwise, RETURN

    Done2:
        return
