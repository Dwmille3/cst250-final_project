# Input arguments:
#	$a0 - starting memory address of the array to be sorted
#	$a1 - number of elements in array

wildcard_sort:
    move $v0, $0 # Holder of the comparison
    li $s1, 1
    # Sort loops

    compare:
        lw $t0, 0($v1)     # previous value
        lw $t1, 4($v1)     # next value
        beq $t0, $t1, no   # If they're equal, jump to no_swap
        nop

        slt $v0, $t0, $t1  # v0 = 1 if t0 < t1, otherwise v0=0
        bne $v0, $0, no    # Don't swap them if v0 is not zero
        nop

        move $s0, $s1      # Swap flag on
        sw $t1, 0($v1)     # Swaping previous and next
        sw $t0, 4($v1)

    bubb:
        move $t5, $s1      # Counter = 1
        move $s0, $0       # A falg that will turn on when a swap happen
        move $v1, $a0      # copying the first adress of the array
        j compare
        nop

    no:
        addiu $v1, $v1,  4     # Make a0 points to next value
        addiu $t5, $t5, 1      # Counter ++
        beq $s0, $s1, going    # if swap flag is off
        nop        # AND

        beq $t5, $a1, done2    # Counter = length, then jump to Done
        nop

    going:                  # Otherwise, it is still sorting, don't terminate
        beq $t5, $a1, bubb  # If counter = array length, jump to sort again
        nop

        j compare           # Jump to compare next
        nop

    done2:
        return
