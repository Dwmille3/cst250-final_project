# Input arguments:
#    $a0 - starting memory address of the array to be sorted
#    $a1 - number of elements in array

bubble_sort:
    # Initializations
    move $v0, $0    # Holder of the comparison
    li $s1, 1       # Decimal number 1
    beq $a1, $s1, Done	# If the length of the array equals one, jump to Done

    # Sort loops
    bubb_sort:
        move $t5, $s1    # Counter = 1
        move $s0, $0     # A falg that will turn on when a swap happen
        move $v1, $a0    # copying the first adress of the array

    compare_next:
        lw $t0, 0($v1)          # previous value
        lw $t1, 4($v1)          # next value
        beq $t0, $t1, no_swap   # If they're equal, jump to no_swap
        nop

        slt $v0, $t0, $t1       # v0 = 1 if t0 < t1, otherwise v0=0
        bne $v0, $0, no_swap    # Don't swap them if v0 is not zero
        nop

        move $s0, $s1     # Swap flag on
        sw $t1, 0($v1)    # Swaping previous and next
        sw $t0, 4($v1)

    no_swap:
        addiu $v1, $v1,  4           # Make a0 points to next value
        addu $t5, $t5, $s1           # Counter ++
        beq $s0, $s1, still_going    # if swap flag is off
        nop                          # AND

        beq $t5, $a1, Done           # Counter = length, then jump to Done
        nop

    still_going:                   # Otherwise, it is still sorting, don't terminate
        beq $t5, $a1, bubb_sort    # If counter = array length, jump to sort again
        nop

        j compare_next             # Jump to compare next
        nop

    Done:
        return
