# Input arguments:
#    $a0 - starting memory address of the array to be sorted
#    $a1 - number of elements in array

insertion_sort:
    # Initializations
    move $v0, $0        # Holder of the comparison
    move $v1, $a0       # copying the  adress of the array to v1
    li $s1, 1           # Decimal number 1
    li $s2, 4           # Decimal number 4
    move $t5, $s1       # Counter = 1
    beq $a1, $s1, Done1	# If the length of the array equals one, jump to Done

    insert_sort:
        lw $t0, 0($a0)         # Previous value
        lw $t1, 4($a0)         # Next value
        beq $t0, $t1, No_swap  # If they're equal, jump to No_swap
        nop

        slt $v0, $t0, $t1      # Is t0 < t1 ?
        bne $v0, $0, No_swap   # If yes, jump to no_swap
        nop

    Again:               # If no
        sw $t1, 0($a0)       # Swap them
        sw $t0, 4($a0)
        subu $a0, $a0, $s2   # a0 points to previous now
        lw $t0, 0($a0)       # load it to t0
        beq $t0, $t1, Again
        nop

        slt $v0, $t0, $t1    # if t0 > t1, it'll keep going backward and swap if not sorted
        beq $v0, $0, Again   # Otherwise, Continue
        nop

        move $a0, $v1        # Resume going forward

    No_swap:
        addu $a0, $a0, $s2     # Add 4 to the address of the array
        addu $v1, $v1, $s2
        addu $t5, $t5, $s1     # Counter ++
        beq $t5, $a1, Done1    # If counter = a1, jump to done
        nop                    # Otherwise

        j insert_sort          # Sort again
        nop

    Done1:
        return
